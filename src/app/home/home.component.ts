import { Component, OnInit, OnDestroy, trigger, state, style, transition, animate, keyframes} from '@angular/core';

import { EventsService }  from '../events.service';
import { DateService }    from '../date.service';
import { TruncatePipe }   from '../truncate.pipe';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('.4s ease-out', style({ opacity: '1' })),
      ]),
    ]),
  ]
})
export class HomeComponent implements OnInit, OnDestroy {


  style: any = [true, false, false, false];
  imageUrl: string = "assets/events/";
  state: string = 'inactive';
  bands = [];
  news = [
    {"title":"In hac habitasse platea dictumst.","date":"16.08.2017","content":"Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.\n\nVestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat."},
    {"title":"In eleifend quam a odio.","date":"12.08.2017","content":"Sed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.\n\nVestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat."},
    {"title":"Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.","date":"14.06.2017","content":"In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\n\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat."},
  ];
  eventsSub: any;


  constructor(
    private eventService: EventsService,
    private dateService: DateService) {}

  ngOnInit() {
    this.sliderRotate();
    this.eventsSub = this.eventService.getConcerts()
      .subscribe((res) => {
          this.sortEvents(res.json());
        });
  }

  sortEvents(e) {
    e.forEach((a) => {
      let d = this.dateService.getDate(a.date);
      a.date = d.date;
      a.shortDate = d.numberPattern;
      a.day = d.shortWeekDay;
    });
    e.sort((a,b) =>  (a.date).getTime() - (b.date).getTime());
    this.bands = e.splice(0,8);
  }

  sliderRotate() {
    let counter = 1;
    setInterval(() => {
      if(counter === this.bands.length) {
        this.style[this.bands.length - 1] = false;
        this.style[0] = true;
        counter = 0;
      } else {
        this.style[counter] = true;
        this.style[counter-1] = false;
      }
      counter++;
    }, 4000);
  }

  ngOnDestroy() {
    this.eventsSub.unsubscribe();
  }

}
