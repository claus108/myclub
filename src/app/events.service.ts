import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable}from 'rxjs';

export interface E {

}

@Injectable()
export class EventsService {

  private url = "assets/data/";

  constructor(private http: Http) {}

  getConcerts() {
    return this.http.get(this.url + "concerts.json");
  }

  getParties() {
    return this.http.get(this.url + "parties.json");
  }
}
