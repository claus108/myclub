import { BrowserModule }           from '@angular/platform-browser';
import { RouterModule, Routes }    from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule }              from '@angular/http';
import { NgModule }                from '@angular/core';

import { AppComponent }            from './app.component';
import { HeaderComponent }         from './header/header.component';
import { AboutComponent }          from './about/about.component';
import { HomeComponent }           from './home/home.component';
import { FooterComponent }         from './footer/footer.component';
import { EventsComponent }         from './events/events.component';
import { EventComponent }          from './event/event.component';
import { NewsComponent }           from './news/news.component';

import { EventsService }           from './events.service';
import { DateService }             from './date.service';
import { TruncatePipe }            from './truncate.pipe';


const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'event/:type/:id', component: EventComponent },
  { path: 'events/:type', component: EventsComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AboutComponent,
    FooterComponent,
    EventsComponent,
    EventComponent,
    TruncatePipe,
    NewsComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [
    EventsService,
    DateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
