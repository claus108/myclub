import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute }               from '@angular/router';

import { EventsService }                from '../events.service';
import { DateService }                  from '../date.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit, OnDestroy {

  type: string;
  id: string
  event;
  events: any[];
  routeSub: any;
  eventSub:any;
  baseUrl = "../../assets/events/";
  backgroundHeight = "100vh";

  constructor(
    private route       : ActivatedRoute,
    private eventService: EventsService,
    private dateService : DateService
    ) { }

  ngOnInit() {
      this.routeSub = this.route.params.subscribe(params => {
        this.type = params['type'];
        this.id = params['id'];
        (this.type === 'party') ? this.getParty(this.id) : this.getConcert(this.id);
       }) ;
  }

  getParty(id) {
    this.eventSub = this.eventService.getParties()
      .subscribe((res) => {
        res.json().forEach((party) => {
          if(party.id === +id) {
            this.setDatePatterns(party);
          }
        })
      });
  }

  getConcert(id) {
    this.eventSub = this.eventService.getConcerts() 
      .subscribe((res) => {
        res.json().forEach((concert) => {
          if(concert.id === +id) {
            this.setDatePatterns(concert);
          }
        });
      });
  }
  
  setDatePatterns(event) {
    this.event = event;
    let d = this.dateService.getDate(this.event.date);
    this.event.door = d.time;
    this.event.day = d.shortWeekDay;
  }


  ngOnDestroy() {
    this.routeSub.unsubscribe();
    this.eventSub.unsubscribe();
  }

}
