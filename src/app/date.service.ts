import { Injectable } from '@angular/core';

@Injectable()
export class DateService {

  daysShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  daysLong = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  constructor() { }

  getDate(d) {
    let date = new Date(d);
    // number of month: eg: November => 11;
    let month = date.getMonth() + 1;
    let monthNumber = (month < 10) ? '0' + month : month;
    // day in month:
    let dayInMonth = date.getDate();
    let dayNumber = (dayInMonth < 10) ? '0' + dayInMonth : dayInMonth; 
    // short numberPattern: 
    let numberPattern = dayNumber + '.' + monthNumber + '.';
    // shortWeekDay pattern: 
    let shortWeekDay = this.daysShort[date.getDay()] + ', ' + numberPattern;
    // monthpattern: 
    let monthPattern = dayNumber + ". " + this.months[month-1];
    // longDayTime:
    let minutes = (date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes();
    let longDayTime = this.daysLong[date.getDay()] + ' ' + date.getHours() + ':' + minutes;
    // time:
    let time = date.getHours() + ':' + minutes;

    return {
      date,
      numberPattern,  // eg: 05.11.
      shortWeekDay,   // eg: Thu, 13.11 
      monthPattern,   // eg: 29.Oct
      longDayTime,    // eg: Sunday 4:45
      time,           // eg: 4:45
    }
  }

}
