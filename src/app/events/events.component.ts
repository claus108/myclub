import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute }    from '@angular/router';

import { EventsService }     from '../events.service';
import { DateService }       from '../date.service'; 


@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit, OnDestroy {

  type: string;
  imageUrl: string = "assets/events/";
  events: any[];
  routeSub: any;
  eventsSub: any;

  constructor(
    private route: ActivatedRoute,
    private eventsService: EventsService,
    private dateService: DateService) { }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      switch(params['type']) {
        case 'concerts':
          this.eventsSub = this.eventsService.getConcerts().subscribe(res => this.sortEvents(res.json()));
          break;
        case 'parties':
          this.eventsSub = this.eventsService.getParties().subscribe(res => this.sortEvents(res.json()));
          break;
        default:
          this.eventsSub = this.eventsService.getParties().subscribe(res => this.sortEvents(res.json()));
      }
    })
  }

  sortEvents(events) {
    events.forEach((event) => {
      let d = this.dateService.getDate(event.date);
      event.date = d.date;
      event.month = d.monthPattern;
      event.day = d.longDayTime;
    });
    events.sort((a,b) =>  (a.date).getTime() - (b.date).getTime());
    this.events = events;
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
    this.eventsSub.unsubscribe();
  }



}